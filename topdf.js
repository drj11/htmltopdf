#!/usr/bin/env node

// Convert HTML to PDF (using PlayWright)
// Usage:
//  node topdf.js myfile.html
// Writes PDF to web.pdf

// https://playwright.dev/docs/library
const playwright = require('playwright')

const path = require('path')

// PDF printing requires chromium
// https://playwright.dev/docs/api/class-page#page-pdf
const launcher = playwright.chromium

const args = process.argv.slice(2)
const filepath = args[0]

;(async () => {
  const options = {}
  if(process.env.CHROMIUM != "") {
    options.executablePath = process.env.CHROMIUM
  }
  const browser = await launcher.launch(options)
  const page = await browser.newPage()
  const p = path.resolve(filepath)
  const out = path.join(path.dirname(p), path.basename(p, '.html')+".pdf")

  await page.goto('file://' + p)

  await page.pdf({
    height: "1116px",  // PDF is in inches, converted at 96px/inch
    landscape: false,
    path: out,
    width: "806px",
  })
  
  await browser.close()
})()

